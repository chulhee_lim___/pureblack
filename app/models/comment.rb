class Comment < ActiveRecord::Base
    
    belongs_to :cafe
    belongs_to :user
    
    mount_uploader :image,CafeimageuploaderUploader
end
