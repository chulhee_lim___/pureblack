class Cafe < ActiveRecord::Base
	
	belongs_to :user
    
    has_many :comments
    has_many :tags
    has_many :payments
    has_many :payinfos
    mount_uploader :image,CafeimageuploaderUploader
    mount_uploader :thumnail,CafeimageuploaderUploader

    geocoded_by :address
    after_validation :geocode
end
