class CafeController < ApplicationController
	
  def cafesearch
    @caves = Cafe.all
  end

  def cafedetail
    @cafe = Cafe.where(:cafehash => params[:hash]).take
    @hash = params[:hash]
    # @comments = @cafe.comments
  end
  
  def cafecreate

    cafe = Cafe.new
    
    cafe.name = params[:cafename]
    cafe.thumnail = params[:thumbnail]
    cafe.image = params[:cafeimage]
    cafe.signaturemenu = params[:signaturemenu]
    cafe.introduction = params[:intro_detail]
    cafe.cafehash = SecureRandom.hex(11)
    cafe.user_id = current_user.id

    cafe.address_road = params[:buyer_addr] + " " + params[:address_detail]
    cafe.address = params[:address_jibun]
    cafe.latitude = params[:latitude]
    cafe.longitude = params[:longitude]
    
    cafe.save

       
    payinfo = Payinfo.new
    payinfo.pay_method = params[:pay_method]
    payinfo.merchant_uid = params[:merchant_uid]
    payinfo.name = params[:name]
    payinfo.paid_amount = params[:paid_amount].to_i
    payinfo.pg_provider = params[:pg_provider]
    payinfo.pg_tid = params[:pg_tid]
    payinfo.apply_num = params[:apply_num]
    payinfo.vbank_num = params[:vbank_num]
    payinfo.vbank_name = params[:vbank_name]
    payinfo.vbank_holder = params[:vbank_holder]
    payinfo.vbank_date = params[:vbank_date]
    payinfo.user_id = params[:user_id].to_i
    payinfo.cafe_id = Cafe.last.id
    payinfo.save

    render :js => "window.location = '/cafe/show_payinfo'"

  end
  
  def caferegister
    @user = User.where(id: current_user.id).take

  end
  
  def add_comment
    
    cafe = Cafe.where(:cafehash => params[:cafehash]).take
    comment = Comment.new
    
    comment.user_id = current_user.id
    comment.content = params[:commentcontent]
    comment.image = params[:commentimage]
    comment.cafe_id = cafe.id
    comment.writtentime = DateTime.now
    comment.hashstr = SecureRandom.hex(11)
    
    comment.save
    
    redirect_to(:back)
  end
  
  def delete_comment
    comment = Comment.where(:hashstr => params[:hash]).take
    comment.destroy
    redirect_to(:back)
  end

  def show_payinfo
    @user = User.where(id: current_user.id).take
    @cafe = Cafe.where(id: current_user.id).take
  end
end