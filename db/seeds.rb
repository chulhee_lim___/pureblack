# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
cafes = Cafe.create([{name: "류",address: "대구 중구 공평동 66-2", address_road: "대구광역시 중구 동성로4길 91"},{name: "카페 루시드",address: "대구 중구 공평동 70-2", address_road: "대구광역시 중구 동성로4길 94"},
								{name: "어썸",address: "대구 중구 봉산동 20-12", address_road: "대구광역시 중구 동성로2길 12-24"},{name: "커피명가 본",address: "대구 중구 삼덕동1가 22-21", address_road: "대구광역시 중구 동성로5길 69"},
								{name: "랄라스윗",address: "대구 중구 삼덕동1가 12-8", address_road: "대구광역시 중구 동성로2길 22"},{name: "아띠",address: "대구 중구 삼덕동1가 22-3", address_road: "대구광역시 중구 동성로5길 77"},
								{name: "인더가든",address: "대구 중구 덕산동 125-18", address_road: "대구광역시 중구 달구벌대로 2109-20"},{name: "안녕 슐츠",address: "대구 중구 동성로3가 115-1", address_road: "대구광역시 중구 동성로1길 29-17"},
								{name: "커피 앤 와플",address: "대구 중구 동성로2가 104", address_road: "대구광역시 중구 동성로 25-1"},{name: "파스쿠찌 대구동성로점",address: "대구 중구 동성로2가 70-1", address_road: "대구광역시 중구 동성로2길 81"}])