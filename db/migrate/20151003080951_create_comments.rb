class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      
      t.string :content              , null: false, default: ""
      t.string :image
      
      t.string :hashstr
      t.datetime :writtentime
      t.integer :user_id
      t.integer :cafe_id
      
      t.timestamps null: false
    end
  end
end
