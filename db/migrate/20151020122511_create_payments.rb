class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      
      t.integer :sort
      t.datetime :paidtime
      
      t.integer :cafe_id
      t.integer :user_id
      
      t.timestamps null: false
    end
  end
end
