class CreateCaves < ActiveRecord::Migration
  def change
    create_table :caves do |t|

	    t.string :name              , null: false, default: ""
      t.string :thumnail
      t.string :address
      t.string :address_road
      t.string :image
      
      t.string :signaturemenu
      t.string :introduction
      t.string :cafehash          , null: false, default: ""
      
      t.integer :user_id
      t.float :latitude #위도
      t.float :longitude #경도
      
      t.timestamps null: false
    end
  end
end
